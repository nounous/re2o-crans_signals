import json


def dhcp_add(zone, host, mac, ip):
    message = json.dumps({
        'action': 'add',
        'zone': zone,
        'host': host,
        'mac': mac,
        'ip': ip,
    })
    print('DHCP', message)

def dhcp_del(zone, host):
    message = json.dumps({
        'action': 'del',
        'zone': zone,
        'host': host,
    })
    print('DHCP', message)

def dns_add(zone, name, type, value):
    message = json.dumps({
        'action': 'add',
        'zone': zone,
        'name': name,
        'type': type,
        'value': value,
    })
    print('DNS', message)

def dns_del(zone, name, type, value):
    message = json.dumps({
        'action': 'del',
        'zone': zone,
        'name': name,
        'type': type,
        'value': value,
    })
    print('DNS', message)

def pre_save_domain(instance, raw, **_kwargs):
    from machines.models import Domain
    try:
        old_instance = Domain.objects.get(pk=instance.pk)
        old_interface = old_instance.interface_parent
    except ObjectDoesNotExist:
        return
    if not old_interface.machine.active or not old_interface.machine.user.has_access():
        return
    interface = instance.interface_parent
    if instance.name != old_instance.name or instance.extension != old_instance.extension:
        dhcp_del(old_instance.extension.name[1:], old_instance.name)
        dns_del(old_instance.extension.name[1:], old_instance.name, 'A', old_interface.ipv4.ipv4)
        for ipv6 in old_interface.ipv6():
            dns_del(old_instance.extension.name[1:], old_instance.name, 'AAAA', ipv6.ipv6)

def post_save_domain(instance, raw, **_kwargs):
    interface = instance.interface_parent
    if not interface.machine.active or not interface.machine.user.has_access():
        return
    dhcp_add(instance.extension.name[1:], instance.name, interface.mac_address, interface.ipv4.ipv4)
    dns_add(instance.extension.name[1:], instance.name, 'A', interface.ipv4.ipv4)
    for ipv6 in interface.ipv6():
        dns_add(instance.extension.name[1:], instance.name, 'AAAA', ipv6.ipv6)

def pre_save_interface(instance, raw, **_kwargs):
    if not instance.machine.active or not instance.machine.user.has_access():
        return
    from machines.models import Interface
    try:
        old_instance = Interface.objects.get(pk=instance.pk)
    except ObjectDoesNotExist:
        return
    if instance.mac_address != old_instance.mac_address or instance.ipv4 != old_instance.ipv4:
        dhcp_del(old_instance.domain.extension.name[1:], old_instance.domain.name)
    if instance.ipv4 != old_instance.ipv4:
        dns_del(instance.domain.extension.name[1:], instance.domain.name, 'A', old_instance.ipv4.ipv4)

def post_save_interface(instance, raw, **_kwargs):
    if not instance.machine.active or not instance.machine.user.has_access():
        return
    dhcp_add(instance.domain.extension.name[1:], instance.domain.name, instance.mac_address, instance.ipv4.ipv4)
    dns_add(instance.domain.extension.name[1:], instance.domain.name, 'A', instance.ipv4.ipv4)

def pre_save_ipv4(instance, raw, **_kwargs):
    pass

def post_save_ipv4(instance, raw, **_kwargs):
    pass

def pre_save_ipv6(instance, raw, **_kwargs):
    if not instance.interface.machine.active or not instance.interface.machine.user.has_access():
        return
    from machines.models import Ipv6List
    domain = instance.interface.domain
    try:
        old_instance = Ipv6List.objects.get(pk=instance.pk)
    except ObjectDoesNotExist:
        return
    if instance.ipv6 != old_instance.ipv6:
        dns_del(domain.extension.name[1:], domain.name, 'AAAA', old_instance.ipv6)

def post_save_ipv6(instance, raw, **_kwargs):
    if not instance.interface.machine.active or not instance.interface.machine.user.has_access():
        return
    domain = instance.interface.domain
    dns_add(domain.extension.name[1:], domain.name, 'AAAA', instance.ipv6)
