from django.apps import AppConfig
from django.db.models.signals import post_save, pre_save

from . import signals


class CransSignalsConfig(AppConfig):
    name = 'crans_signals'

    def ready(self):
        pre_save.connect(
            signals.pre_save_domain,
            sender='machines.Domain',
        )
        post_save.connect(
            signals.post_save_domain,
            sender='machines.Domain',
        )
        pre_save.connect(
            signals.pre_save_interface,
            sender='machines.Interface',
        )
        post_save.connect(
            signals.post_save_interface,
            sender='machines.Interface',
        )
        pre_save.connect(
            signals.pre_save_ipv6,
            sender='machines.Ipv6List',
        )
        post_save.connect(
            signals.post_save_ipv6,
            sender='machines.Ipv6List',
        )
